import React from 'react';
import './App.scss';
import Main from "./components/Main";


function App () {
    return (
        <div>
            <h1 className="title">Tick Tack Toe</h1>
            <div className="App">
                <Main/>
            </div>
        </div>
    );
}

export default App;

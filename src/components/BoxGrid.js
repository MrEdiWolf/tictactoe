import React, {Component} from 'react';
import GameStatus from "./GameStatus";
import Box from "./Box";

export default class BoxGrid extends Component {

    constructor(props) {
        super(props);
    }

    selectField(index, selectedMark) {
        this.props.selectFieldByPlayer(index, selectedMark)
    }

    resetGame() {
        this.props.resetGame()
    }

    render() {
        return (
            <div>
                {
                    this.props.gameStatus && <GameStatus gameStatus={this.props.gameStatus}/>
                }
                <div className="board-wrapper">
                    <div className="board">
                        {
                            this.props.boxes.map((box, index) =>
                                <div key={index}
                                     onClick={() => this.selectField(index, this.props.selectedMark)}
                                >
                                    <Box boxValue={box}/>
                                </div>
                            )
                        }
                    </div>
                </div>
                <button className="new-game-btn" onClick={() => this.resetGame()}>Nowa gra</button>
            </div>
        )
    }
}

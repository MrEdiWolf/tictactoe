import React, {Component} from 'react';
import MarkSelector from "./MarkSelector";
import axios from 'axios';
import Config from '../config';
import BoxGrid from "./BoxGrid";

export default class Main extends Component {

    constructor (props) {
        super(props);
        this.state = {
            boxes: [
                null, null, null,
                null, null, null,
                null, null, null
            ],
            gameStatus: "",
            selectedMark: ""
        }
    }

    componentDidMount () {
        this.checkWinner();
    }

    winLines = [
        ["0", "1", "2"],
        ["3", "4", "5"],
        ["6", "7", "8"],
        ["0", "3", "6"],
        ["1", "4", "7"],
        ["2", "5", "8"],
        ["0", "4", "8"],
        ["2", "4", "6"],
    ];

    resetGame = () => {
        axios.post(Config.API_URL + '/reset-game')
            .then(response => {
                this.setState({gameStatus: null});
                this.setState({selectedMark: null});
                this.setState({boxes: response.data});
            })
    };

    getResultFromBackend = () => {
        axios.get(Config.API_URL + '/result')
            .then(response => {
                this.setState({boxes: response.data});
                this.checkWinner();
            })
    };

    setFieldByAI = () => {
        axios.post(Config.API_URL + '/set-field-by-ai', {'mark': this.state.selectedMark === 'X' ? 'O' : 'X'})
            .then(() => {
                this.getResultFromBackend();
            })
    };

    sendSelectedFieldToBackend = (index) => {
        let x = index % 3;
        let y = Math.floor(index / 3);
        this.sendFieldToBackend(x, y, this.state.selectedMark);
    };

    sendFieldToBackend = (x, y, mark) => {
        let dataToSend = {
            "fieldX": x,
            "fieldY": y,
            "mark": mark
        };
        axios.post(Config.API_URL + '/set-field-by-user', dataToSend).then(response => {
            this.setState({boxes: response.data});
            const updatedBoxes = response.data;
            if (updatedBoxes.every(box => box != null)) {
                this.checkWinner();
            } else {
                this.setFieldByAI();
            }
        }).catch(() => {
            console.log('Podane pole nie jest puste!');
        })
    };

    checkWinner = () => {
        this.winLines.forEach(winLine => {
            const [a, b, c] = winLine;
            if (this.state.boxes[a] && (this.state.boxes[a] === this.state.boxes[b]) && (this.state.boxes[b] === this.state.boxes[c])) {
                if (this.state.boxes[a] === this.state.selectedMark) {
                    return this.setState({gameStatus: 'win'});
                } else {
                    return this.setState({gameStatus: 'lose'});
                }
            } else if (this.state.boxes.every(box => box != null) && this.state.gameStatus === '') {
                return this.setState({gameStatus: 'draw'});

            }
        });
    };

    selectFieldByPlayer = (index, mark) => {
        if (!this.state.boxes[index]) {
            let tempState = [...this.state.boxes];
            tempState[index] = mark;
            this.setState({
                boxes: tempState
            });
            this.sendSelectedFieldToBackend(index);
        }
    };

    render () {
        return (
            <div>
                {
                    !this.state.selectedMark
                        ? <MarkSelector onMarkChange={(mark) => {
                            this.setState({selectedMark: mark})
                        }}/>
                        : <BoxGrid gameStatus={this.state.gameStatus}
                                   boxes={this.state.boxes}
                                   resetGame={() => this.resetGame()}
                                   selectFieldByPlayer={(index, mark) => this.selectFieldByPlayer(index, mark)}
                                   selectedMark={() => this.state.selectedMark}
                        />
                }
            </div>
        )
    }
}

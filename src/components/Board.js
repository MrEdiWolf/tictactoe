import React, {useEffect, useState} from 'react';
import Box from './Box'
import axios from 'axios';
import Config from '../config';

//board with boxes ( matrix )
export default function Board () {

    const [boxes, setBoxes] = useState([
        null, null, null,
        null, null, null,
        null, null, null
    ]);

    let [gameStatus, setStatus] = useState('');
    let [selectedMark, setMark] = useState('');

    let winLines = [
        ["0", "1", "2"],
        ["3", "4", "5"],
        ["6", "7", "8"],
        ["0", "3", "6"],
        ["1", "4", "7"],
        ["2", "5", "8"],
        ["0", "4", "8"],
        ["2", "4", "6"],
    ];

    const resetGame = () => {
        axios.post(Config.API_URL + '/reset-game')
            .then(response => {
                setStatus(null);
                setMark(null);
                setBoxes(response.data);
            })
    };

    const getResultFromBackend = () => {
        axios.get(Config.API_URL + '/result')
            .then(response => {
                setBoxes(response.data);
            })
    };

    useEffect(() => {
        checkWinner();
    }, [boxes]);

    const setFieldByAI = () => {
        axios.post(Config.API_URL + '/set-field-by-ai', {'mark': selectedMark === 'X' ? 'O' : 'X'})
            .then(() => {
                getResultFromBackend();
            })
    };

    const sendSelectedFieldToBackend = (index) => {
        let x = index % 3;
        let y = Math.floor(index / 3);
        sendFieldToBackend(x, y, selectedMark);
    };

    const sendFieldToBackend = (x, y, mark) => {
        let dataToSend = {
            "fieldX": x,
            "fieldY": y,
            "mark": mark
        };
        axios.post(Config.API_URL + '/set-field-by-user', dataToSend).then(response => {
            setBoxes(response.data);
            const updatedBoxes = response.data
            if (updatedBoxes.every(box => box != null)) {
                checkWinner();
            } else {
                setFieldByAI();
            }
        }).catch(() => {
            console.log('Podane pole nie jest puste!');
        })
    };

    const checkWinner = () => {
        console.log(boxes);
        winLines.forEach(winLine => {
            const [a, b, c] = winLine;
            if (boxes[a] && (boxes[a] === boxes[b]) && (boxes[b] === boxes[c])) {
                if (boxes[a] === selectedMark) {
                    return setStatus('win');
                } else {
                    return setStatus('lose')
                }
            } else if (boxes.every(box => box != null) && gameStatus === '') {
                setStatus('draw');
            }
        });
    };

    const selectFieldByPlayer = (index, mark) => {
        if (!boxes[index]) {
            boxes[index] = mark;
            sendSelectedFieldToBackend(index);
        }
    };

    const Boxes = boxes.map((box, index) =>
        <div onClick={() => selectFieldByPlayer(index, selectedMark)}>
            <Box boxValue={box} index={index}/>
        </div>
    );

    const MarkSelector = <div className="sign-select">
        <p onClick={() => test()}>Wybierz znak</p>
        <button onClick={() => setMark('X')}>X</button>
        <button onClick={() => setMark('O')}>O</button>
    </div>;

    const GameBoard = <div>
        {gameStatus && <div className="game-result">
            {gameStatus === 'win' && <div className="win">Wygrałeś!</div>}
            {gameStatus === 'lose' && <div className="lose">Przegrałeś!</div>}
            {gameStatus === 'draw' && <div className="draw">Remis!</div>}
        </div>}
        <div className="board-wrapper">
            <div className="board">
                {Boxes}
            </div>
        </div>
        <button className="new-game-btn" onClick={() => resetGame()}>Nowa gra</button>
    </div>;

    return (
        <div>
            {!selectedMark ? MarkSelector : GameBoard}
        </div>
    )
}
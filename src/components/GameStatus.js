import React, {Component} from "react";

export default class GameStatus extends Component {
    constructor (props) {
        super(props);
        this.state = {
            gameStatus: ""
        }
    }

    render () {
        return (
            <div className="game-result">
                {this.props.gameStatus === 'win' && <div className="win">Wygrałeś!</div>}
                {this.props.gameStatus === 'lose' && <div className="lose">Przegrałeś!</div>}
                {this.props.gameStatus === 'draw' && <div className="draw">Remis!</div>}
            </div>
        )
    }

}

import React, {Component} from "react";

export default class MarkSelector extends Component {
    constructor (props) {
        super(props);
    }

    selectMark(mark) {
        this.props.onMarkChange(mark);
    }

    render() {
        return (
            <div className="sign-select">
                <p>Wybierz znak</p>
                <button onClick={() => this.selectMark('X')}>X</button>
                <button onClick={() => this.selectMark('O')}>O</button>
            </div>
        )
    }
}

class Board {
    constructor () {
        this.result = [
            null, null, null,
            null, null, null,
            null, null, null
        ]
    }

    selectFieldByUser(x, y, mark) {
        let fieldToSelect = x + (3 * y); // converting coordinates to linear array
        if (!this.result[fieldToSelect]) {
            this.result[fieldToSelect] = mark;
            return true; // field is selected
        } else {
            return false; // field is not selected because not empty
        }
    };

    selectFieldByAI(mark) {
        let fieldIndexToSelect = Math.floor(Math.random() * 9);
        while (this.result[fieldIndexToSelect]) {
            fieldIndexToSelect = Math.floor(Math.random() * 9);
        }
        this.result[fieldIndexToSelect] = mark;
    };

    clearFields() {
        this.result = this.result.map(field => field = null);
    }
}

module.exports = Board;

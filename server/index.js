const express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
var Board = require('./Board');

var board = new Board();

app.use(cors({
    origin: ['http://localhost:3000'],
    credentials: false
}));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
// przepuszczenie zapytań z innego portu
app.options('*', cors()); // allowing to send requests from other port or domain


app.get('/', (req, res) => {
    res.status(200).send('Server is working...')
});

app.get('/result', cors(), (req, res) => {
    console.log('getting result')
    res.status(200).send(board.result); // sending current fields
});

app.post('/reset-game', (req, res) => {
    board.clearFields();
    res.status(200).send(board.result); // clearing fields
});

app.post('/set-field-by-user', (req, res) => {
    const x = req.body.fieldX;
    const y = req.body.fieldY;
    const mark = req.body.mark;
    if (board.selectFieldByUser(x, y, mark)) { // if field is selected
        res.status(200).send(board.result);
    } else { // if field is not selected
        res.status(400).send('Podane pole nie jest puste');
    }
});

app.post('/set-field-by-ai', (req, res) => {
    console.log('setting field by ai result')
    const mark = req.body.mark;
    board.selectFieldByAI(mark);
    res.status(200).send(board.result);
});


app.listen(5000, () => console.log('Server is working on port 5000'));
